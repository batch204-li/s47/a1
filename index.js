// Directions:
// Create two event listeners for when a user types in the first and last name inputs
// When this event triggers, update the span-full-name's content to show the value of the first name input on the left and the value of the last name input on the right

// Strecth goal: Instead of an anonymous function, create a new function that the two event listeners will call

// Guide question: Where do the names come from and where should they go

let input1 = document.querySelector('#txt-first-name');
let input2 = document.querySelector('#txt-last-name');
let span1 = document.querySelector('#span-full-name');

input1.addEventListener('keyup', () => {
	myFunc()
})

input2.addEventListener('keyup', () => {
	myFunc()
})

function myFunc() {
	span1.textContent = (input1.value + " " + input2.value);
	console.log(event.target.value);
}

